import smtplib, ssl
import json

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email.headerregistry import Address
import argparse, sys


def get_credentials(from_nickname):
    with open('config.json') as json_file:
        data = json.load(json_file)
    
    return data[from_nickname]

def get_body(file_body):
    with open(file_body) as bodyFile:
        body = bodyFile.read()
    return body

def sendMail(credetials=None,specialFromHeader=None, subject="Subject",to=[],cc=[], body="body", attatchments=[]):
    
    print("constructing email")
    msg = MIMEMultipart()
    if specialFromHeader:
        msg['From'] = specialFromHeader + " <" + credentials['email'] + '>'
    else:
        msg['From'] = credetials['email']

    msg['To'] = COMMASPACE.join(to)
    msg['CC'] = COMMASPACE.join(cc)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject
    
    if credetials['reply-to']:
        msg.add_header("reply-to", credetials['reply-to'])

    msg.attach(MIMEText(body))

    print("attatching files")
    for f in attatchments or []:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        msg.attach(part)




    to_send_to = to + cc


    print("logging in")

    smtp_server = "smtp.gmail.com"
    port = 587  # For starttls
    #context = ssl.create_default_context()
    context = ssl._create_unverified_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(credentials['email'], credentials['password'])
        print("sending mail to(includes cc): ", to_send_to)

        server.sendmail(credentials['email'], to_send_to, msg.as_string())

        print("Success!")
    


parser = argparse.ArgumentParser(description='TDescription')
parser.add_argument("-frm", help="From email credentials nickname listed in config.json", metavar='from_email')
parser.add_argument("-to", help="Comma delimited list of recievers", metavar='reciever_list')
parser.add_argument('-cc', help='Send simple message', metavar='carbon_copy')
parser.add_argument("-sub", help="Mail subject", metavar='subject')
parser.add_argument("-body", help="Text file for body", metavar='body')
parser.add_argument("-att", help="Comma delimited of attatched files", metavar='files', default="")
parser.add_argument("-alias", help="To alias", metavar='special_header', default=None)
args = parser.parse_args()

if __name__ == "__main__":
    if args.frm:
        if not args.to:
            print("Please specify -t")
            exit()
        if not args.sub:
            print("Please specify -s")
            exit(0)
        if not args.body:
            print("Please specify -b")
            exit(0)


        print("Getting credentials for", args.frm)
        credentials = get_credentials(args.frm)

        attatchmentList = args.att.split(',')
        toList = args.to.split(',')

        body = get_body(args.body)

        if args.cc:
            ccList = args.cc.split(',')
        else:
            ccList = []


        sendMail(credetials=credentials,
                 specialFromHeader=args.alias, 
                 subject=args.sub,
                 to=toList,
                 cc=ccList, 
                 body=body, 
                 attatchments=attatchmentList)

        
    elif not ('-h' in sys.argv):
            print('Use -h for all options.')

