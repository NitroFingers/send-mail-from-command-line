# Send Mail from command line

commandline email sender with attachments, aliases, multi cc and to addresses



usage: sendMail.py [-h] [-frm from_email] [-to reciever_list]
                   [-cc carbon_copy] [-sub subject] [-body body] [-att files]
                   [-alias special_header]

TDescription

optional arguments:
  -h, --help            show this help message and exit
  -frm from_email       From email credentials nickname listed in config.json
  -to reciever_list     Comma delimited list of recievers
  -cc carbon_copy       Send simple message
  -sub subject          Mail subject
  -body body            Text file for body
  -att files            Comma delimited of attatched files
  -alias special_header
                        To alias